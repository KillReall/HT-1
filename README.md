# HT 1

1.как Long работает в 32 битных системах?
в 32 битной системе не существует типа long (т.к. он 64разрядный),его заменяют сдвоенным значением типа int


2.Как сравниваются элементы коллекций?
через метод equals

3.Почему Мар не относится к Collection?
В интерфейсе Collection описан метод add(Object o).
Также, Map имеют методы keySet, valueSet, которых нет в Collections
соответственно они не совместимыю


4.Что такое итератор и как он работает в коллекциях?
Iterator - интерфейс, для организации цикла для перебора коллекций,
который путем проверки следующих элементов коллекции может пройти по массиву.
hasNext - true, если есть еще элементы
next - возвращает слудующий элемент



5.Как можно перебрать все значения HashMap?
с помощью интерфейса Iterator 
Iterator<Map.Entry<Integer, String>> itr = map.entrySet().iterator();
while(itr.hasNext()) {
   Map.Entry<Integer, String> entry =  itr.next();
   // get key
   Integer key = entry.getKey();
   // get value
   String value = entry.getValue();




6.Что будет если объект который выступал в качестве ключа в структуре MAP поменяется?
Мы не сможем предоставить доступ к значению, которое хранится под этим ключом.

7.Как перебрать все значения HashMap?
 все в ответе 5)

8.Разница между Iterable и Iterator.
Iterator механизм перебора значений коллекции 
Iterable интерфейс показывающий существование реализации Iterator в классе 

9.Что будет если вызвать Iterator.next()
next - возвращает слудующий элемент
10.Что будет если добавить еще один элемент в коллекцию во время работы итератора
Поведение итератора не определено, если базовая коллекция изменяется во время 
выполнения итерации любым способом, кроме вызова этого метода.

Задача 1

class Test {
 
  public static void main(String[ ] args) {
  byte a = 15;
  byte b = a;
  System.out.println(b);
  }
}

Ответ: 15

Задача 2
class Test {
  public static void main(String[ ] args) {
  double a=128;
  byte b=(byte)a;
  System.out.println(b);
  }
}

Ответ: -128

Задача 3
public class Test {
 public static void main(String[ ] args){ 	
  char v2 = '\u0031';
  char v1 = '2';     	// '\u0032';
  char v3 = 50;      	// '\u0032';
    System.out.println(v1 + v2 + v3);
	}
}
 

Ответ: -149

Задача 3
public class Test {
public static void main(String[] args){ 	
  char v2 = '\u0031';
  char v1 = '2';     	// '\u0032';
  char v3 = 50;      	// '\u0032';
    	System.out.println(‘’+v1 + v2 + v3);
	}
}
 

Ответ: Ошибка компиляции

Задача 4
Сравните 2 фрагмента кода:
 
  1:   int v1=1; long v2=2; v1=v1+v2;
 Ответ: ошибка компиляции -  “несовместимые типы: возможное преобразование с потерями из long в int”

  2:   int v1=1; long v2=2; v1+=v2;
 Ответ: 3


Задача 5
Какой результат выполнения данного кода?
 
  int x=1;
  print("x="+x);      	// System.out.print
  print(1+2+"text");
  print("text"+1+2);

Ответ: выводим на экран x=13texttext12
