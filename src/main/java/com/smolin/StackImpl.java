package com.smolin;

import java.util.*;


public class StackImpl extends Stack<Integer> {
    int[] arr;
    int index;


    public StackImpl() {
        this.arr = new int[10];
        this.index = -1;
    }


    @Override
    public Integer peek() {
        int currentPos = this.arr[index];
        return currentPos;
    }

    @Override
    public void clear() {
        this.arr = new int[10];
        this.index = -1;
    }

    @Override
    public Integer push(Integer o) {
        index++;
        if (index >= arr.length) {
            this.arr = Arrays.copyOf(arr, arr.length * 2);
        }
        this.arr[index] = o;
        return o;
    }

    @Override
    public Integer pop() {
        int lastPos = this.arr[index];
        arr[index] = 0;
        index--;
        return lastPos;

    }

    @Override
    public int size() {
        return index + 1;
    }

    @Override
    public boolean isEmpty() {
        if (index == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public synchronized int search(Object o) {
        if(o instanceof Integer) {
            for (int i = 0; i <= index; i++) {
                if (o.equals(this.arr[i])){
                    return i;
                }
            }

            throw new NoSuchElementException("Элемент в массиве не найден");
        } else {
            throw new ClassCastException("Передайте Integer переменную");
        }
    }
}
