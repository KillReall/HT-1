import com.smolin.StackImpl;
import org.junit.Assert;


class StackImplTest {

    private StackImpl init(int[] args) {
        StackImpl stack = new StackImpl();
        for (int i = 0; i < args.length; i++) {
            stack.push(args[i]);
        }
        return stack;
    }

    @org.junit.jupiter.api.Test
    void peek() {
        int args[] = {2, 4, 6, 1};
        StackImpl stack = init(args);
        stack.peek();
        Assert.assertEquals(1, (int) stack.peek());
    }

    @org.junit.jupiter.api.Test
    void clear() {
        StackImpl stack = init(new int[]{2, 5, 1});
        stack.clear();
        Assert.assertEquals(0, stack.size());
    }

    @org.junit.jupiter.api.Test
    void push() {
        StackImpl stack = new StackImpl();
        stack.push(5);
        Assert.assertEquals(5, (int) stack.peek());
        stack.push(6);
        Assert.assertTrue(stack.size() == 2);
    }

    @org.junit.jupiter.api.Test
    void pop() {
        StackImpl stack = init(new int[]{2, 5, 9});
        stack.pop();
        Assert.assertEquals(5, (int) stack.pop());

    }

    @org.junit.jupiter.api.Test
    void size() {
        StackImpl stack = init(new int[]{2, 5, 12});
        Assert.assertEquals(3, stack.size());
    }

    @org.junit.jupiter.api.Test
    void isEmpty() {
        StackImpl stack = new StackImpl();
        stack.isEmpty();
        Assert.assertFalse(stack.isEmpty());
        stack.push(12);
        Assert.assertTrue(stack.isEmpty());
    }

    @org.junit.jupiter.api.Test
    void search() {
        StackImpl stack = init(new int[]{2, 5, 1});
        Assert.assertEquals(1, stack.search(5));

    }
}